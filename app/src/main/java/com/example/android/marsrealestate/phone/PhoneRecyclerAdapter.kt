package com.example.android.marsrealestate.phone

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.databinding.PhoneItemBinding
import com.example.android.marsrealestate.phone.model.Phone

class PhoneRecyclerAdapter(private val onClickListenerPhone: OnClickListenerPhone) : ListAdapter<Phone, PhoneRecyclerAdapter.PhonePropertyviewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhonePropertyviewHolder {
        return PhonePropertyviewHolder(PhoneItemBinding.inflate(LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: PhonePropertyviewHolder, position: Int) {
        val phoneProperty = getItem(position)
        holder.itemView.setOnClickListener {
            onClickListenerPhone.onClick(phoneProperty)
        }
        holder.bind(phoneProperty)
    }

    companion object DiffCallback : DiffUtil.ItemCallback<Phone>() {
        override fun areItemsTheSame(oldItem: Phone, newItem: Phone): Boolean {
            return oldItem === newItem
        }

        override fun areContentsTheSame(oldItem: Phone, newItem: Phone): Boolean {
            return oldItem.id == newItem.id
        }
    }

    class PhonePropertyviewHolder(private var binding: PhoneItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(phone: Phone) {
            binding.property = phone
            binding.executePendingBindings()
        }

    }

    class OnClickListenerPhone(val clickListener: (phone: Phone) -> Unit) {
        fun onClick(phone: Phone) = clickListener(phone)
    }
}