package com.example.android.marsrealestate


import com.example.android.marsrealestate.phone.model.Phone
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query


private const val BASE_URL = "http://q11.jvmhost.net/"


private val moshi = Moshi.Builder().add(KotlinJsonAdapterFactory()).build()

object RetrofitApi {

    private val retrofit = Retrofit.Builder()
        .baseUrl(BASE_URL) // 1 URI
        .addConverterFactory(MoshiConverterFactory.create(moshi)) // 2 converter factory
        .addCallAdapterFactory(CoroutineCallAdapterFactory()) // !! CoroutineCallAdapterFactory allows us returns  Deferred object  .
        .build()

    val retrofitApiService: RetrofitApiService by lazy {
        retrofit.create(RetrofitApiService::class.java)
    }
}




interface RetrofitApiService {

    @GET("phone_json")
    fun getPhoneList(): Deferred<List<Phone>>

}
