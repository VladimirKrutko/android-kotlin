package com.example.android.marsrealestate.phone

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.android.marsrealestate.RetrofitApi
import com.example.android.marsrealestate.phone.model.Phone
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch


enum class PhoneApiStatus {
    LOADING, ERROR, DONE
}

class PhoneViewModel : ViewModel() {


    val phones: LiveData<List<Phone>>
        get() = _phones
    private val _phones = MutableLiveData<List<Phone>>()

    val status: LiveData<PhoneApiStatus>
        get() = _status
    private val _status = MutableLiveData<PhoneApiStatus>()


    val navigateToSelectedPhone: LiveData<Phone> get() = _navigateToSelectedPhone
    private val _navigateToSelectedPhone = MutableLiveData<Phone>()


    private var viewModelJob = Job()
    private val coroutineScope = CoroutineScope(viewModelJob + Dispatchers.Main)


    init {
        getPhoneProperties()
    }

    private fun getPhoneProperties() {

        coroutineScope.launch {

            var getPropertiesDeferred = RetrofitApi.retrofitApiService.getPhoneList()

            try {
                _status.value = PhoneApiStatus.LOADING
                var listResult = getPropertiesDeferred.await()
                _status.value = PhoneApiStatus.DONE
                _phones.value = listResult
            } catch (e: Exception) {
                _status.value = PhoneApiStatus.ERROR
                Log.d("Error dm - > ", "Internet connection not exist or Server is shot down")
                _phones.value = ArrayList()
            }

        }

    }


    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel() // !! Important
    }


}