/*
 * Copyright 2019, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package com.example.android.marsrealestate

import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.akordirect.vmccnc.R
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.example.android.marsrealestate.lathe.Lathe
import com.example.android.marsrealestate.lathe.LatheApiStatus
import com.example.android.marsrealestate.lathe.LathePhotoGridAdapter
import com.example.android.marsrealestate.network.MarsProperty
import com.example.android.marsrealestate.overview.MarsApiStatus
import com.example.android.marsrealestate.overview.PhotoGridAdapter
import com.example.android.marsrealestate.vmc.VmcApiStatus
import com.example.android.marsrealestate.vmc.VmcPhotoGridAdapter
import com.example.android.marsrealestate.vmc.VmcProperty

/**
 * When there is no Mars status data (data is null), hide the [RecyclerView], otherwise show it.
 */
@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<MarsProperty>?) {
    val adapter = recyclerView.adapter as PhotoGridAdapter
    adapter.submitList(data)
}


@BindingAdapter("imageUrl")
fun bindImage(imgView: ImageView, imgUrl: String?) {
    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("https").build()
        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}



@BindingAdapter("marsApiStatus")
fun bindStatus(statusImageView: ImageView, status: MarsApiStatus?) {
    when (status) {
        MarsApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        MarsApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        MarsApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}



////        ------------- For VMC  --------------------       ////


@BindingAdapter("listDataVmc")
fun bindRecyclerViewVmc(recyclerView: RecyclerView, data: List<VmcProperty>?) {
    val adapter = recyclerView.adapter as VmcPhotoGridAdapter
    adapter.submitList(data)
}



@BindingAdapter("imageVmc")
fun bindImageVmc(imgView: ImageView, imgUrl: String?) {

    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("http").build()
        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}

@BindingAdapter("vmcApiStatus")
fun bindStatusVmc(statusImageView: ImageView,  status: VmcApiStatus?) {
    when (status) {
        VmcApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        VmcApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        VmcApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

////        --- ---- --- --- F o r  LATHE  --- --- --- --- --- --- ---       ////


@BindingAdapter("listDataLathe")
fun bindRecyclerViewLathe(recyclerView: RecyclerView, data: List<Lathe>?) {
    val adapter = recyclerView.adapter as LathePhotoGridAdapter
    adapter.submitList(data)
}


@BindingAdapter("imageUrlLathe")
fun bindImageLathe(imgView: ImageView, imgUrl: String?) {

    imgUrl?.let {
        val imgUri = imgUrl.toUri().buildUpon().scheme("http").build()

        Glide.with(imgView.context)
                .load(imgUri)
                .apply(RequestOptions()
                        .placeholder(R.drawable.loading_animation)
                        .error(R.drawable.ic_broken_image))
                .into(imgView)
    }
}

@BindingAdapter("latheApiStatus")
fun bindStatusLathe(statusImageView: ImageView,  status: LatheApiStatus?) {
    when (status) {
        LatheApiStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        LatheApiStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_connection_error)
        }
        LatheApiStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}