package com.example.android.marsrealestate.vmc

import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.squareup.moshi.Moshi
import retrofit2.Call
import retrofit2.Retrofit
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import kotlinx.coroutines.Deferred
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


enum class VmcApiFilter(val value: String) {
    AXES_3("3"),
    AXES_3_PLUS_2("3+2"),
    AXES_4("4"),
    AXES_5("5"),
    AXES_ALL("all")
}

private const val BASE_URL = "http://q11.jvmhost.net/"

private val moshi = Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()

private val retrofit = Retrofit.Builder()
        .addConverterFactory(MoshiConverterFactory.create(moshi))
        .addCallAdapterFactory(CoroutineCallAdapterFactory())
        .baseUrl(BASE_URL)
        .build()


interface VmcApiService {
    @GET("vmc_json")
//    fun getProperties(): Deferred<List<VmcProperty>> //Call<String>
    fun getProperties(@Query("axes") type: String): Deferred<List<VmcProperty>> //Call<String>

}


object VmcApi{
    val retrofitService : VmcApiService by lazy {
        retrofit.create(VmcApiService::class.java)
    }
}